### Entropy
<img src="ExtraFiles/PhotoFiles/AIFigure8.png" width = "500" align=center /> 

### Information Gain
<img src="ExtraFiles/PhotoFiles/AIFigure7.png" width = "500" align=center />  <br>

---

### Decision Tree
ID3 ( Iterative Dichotomiser 3 )

---

### Probability Density Functions : use Gaussian distribution
<!-- <div  align="center">   -->
<img src="ExtraFiles/PhotoFiles/AIFigure4.png" width = "260" align=center />  
</div>   
<br> u = mean(avg)  
<br> Variance:<img src="ExtraFiles/PhotoFiles/AIFigure5.png" width = "400" align=center />
<br> <img src="ExtraFiles/PhotoFiles/AIFigure6.png" width = "260" align=center /> 
<br> |V| is the number of elements in V

---

### Laplace Smoothing : +1 value for each sample, to guarantee no 0 effect the result  

---

### Distributions on Multiple Discrete or Continuous Random Variables  
<!-- <div  align="center">   -->
<img src="ExtraFiles/PhotoFiles/AIFigure3.png" width = "350" align=center />
</div>

---

### summation or product of several items  
<!-- <div  align="center">   -->
<img src="ExtraFiles/PhotoFiles/AIFigure2.png" width = "200" align=center />  
</div>  
  
---

<!-- <div  align="center"> -->
<img src="ExtraFiles/PhotoFiles/AIFigure1.png" width = "300" align=center />  
</div>

<br>
<img src="ExtraFiles/PhotoFiles/AI1-0-1.png" width = "500" align=center />  
