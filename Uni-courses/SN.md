TLS  (Transport Layer Security)  安全传输层协议 <br>
	TLS Record	记录协议 <br>
	TLS Handshake 	握手协议 <br>
AES (Advanced Encryption Standard) 高级加密标准 (对称加密算法 en&de same key) <br>
	C-密文	E-加密函数	K-密钥	P-明文 <br>
	C  = E (K , P)       AES加密 <br>
	P-明文	D-解密函数	K-密钥	C-密文 <br>
	P = D (K , C)       AES解密 <br>
	加密长度只能为128位，即每个分组16个字节，每个字节8位 <br> 
														一个字母 = 1 byte = 8 bit（位）<br>
	
drwxrwxrwx <br>
	r表示read，读取权限，代表数字4 <br>
	w表示write，写入权限，代表数字2 <br>
	x表示execute，执行权限，代表数字1 <br>
linux中rwx权限前的c和d都表示： <br>
	-就是普通的文件 <br>
	d表示是目录 <br>
	c表示是字符设备(在linux/unix，所有的设备都是文件) <br>
	b是块设备文件 <br>
	s是socket文件，等… <br>

<img src="ExtraFiles/PhotoFiles/SN-0-1.png" width = "800" align=center />  <br>
<img src="ExtraFiles/PhotoFiles/SN-0-3.png" width = "300" align=center />  <br>
