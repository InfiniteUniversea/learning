## Gödel numbers:  

<div  align="center" >  
<img src="ExtraFiles/PhotoFiles/math0-0.png" width = "200" align=center />
<img src="ExtraFiles/PhotoFiles/math0-1.png" width = "211" align=center />
</div> 

> #### Gödel's incompleteness theorems 
> <details><summary>Video link</summary>
> https://www.youtube.com/watch?v=HeQX2HjkcNo  (EN)
> https://www.bilibili.com/video/BV1464y1k7Ya  (CN) 
> </details> 

## _natural numbers(N)_ begin with zero : 0,1,2,3,4,...  

<div  align="center" >    
<img src="ExtraFiles/PhotoFiles/math1.png" width = "600" align=center />
</div> 

<div  align="center" >    
<img src="ExtraFiles/PhotoFiles/math2.png" width = "600" align=center />
</div> 

<details><summary>Gödel's incompleteness theorems</summary>
https://en.wikipedia.org/wiki/G%C3%B6del%27s_incompleteness_theorems <br>
https://plato.stanford.edu/entries/goedel-incompleteness/
</details>

`denary`  (10)  
`binary`  (2)  

## _integers(Z)_ : ...,−3,−2,−1,0,1,2,3,...
<div  align="center" >    
<img src="ExtraFiles/PhotoFiles/math3.png" width = "600" align=center />
</div> 
