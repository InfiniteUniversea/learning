##### Unary operator &  
`p = &c;`  
assigns the **address** of c to p, and `p` is said to **"point to"** `c`.

##### Unary operator *  
If p is a pointer to an integer object, say `c=5`, then `*p` will give the value of `c`, i.e. 5 So,  
`p = &c;`  
`c = *p;`  

---

#### Decimal system (10)  
- 0,1,2,3,4,5,6,7,8,9   

957 = 9 x 10^2 + 5 x 10^1 + 7 x 10^0  

#### Binary system (2)
- 0,1  digit is called ‘**bit**’  

111 = 1 x 2^2 + 1 x 2^1 + 1 x 2^0 = 7 **in decimal**

#### Hexadecimal system (16)
- 0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F  

A3B = A x 16^2 + 3 x 16^1 + B x 16^0 = 10 x 16^2 + 3 x 16^1 + 11 x 16^0 = 2114 **in decimal**

<img src="ExtraFiles/PhotoFiles/Cfigure8.png" width = "250" align=center />

- ### 8bit(2) = 2Hex digit(16) = 1byte  

---

2 Declaration of **array**  
with **3 rows** and **4 columns**  
`int a[3][4];`

---

**scanf()** function can be used to receive inputs from keyboard.  
It is defined in **stdio.h** library and prototype is:  
`scanf("%format", &variable_name); `   
_notice: `%` `&`_

**printf()** function can be used to print outputs.  
It is defined in **stdio.h** library and prototype is:  
`printf("%format", variable_name);`  
_notice: `%`_

<img src="ExtraFiles/PhotoFiles/Cfigure1.png" width = "350" align=center />  <br>
<img src="ExtraFiles/PhotoFiles/Cfigure4.png" width = "350" align=center />  <br>
<img src="ExtraFiles/PhotoFiles/Cfigure2.png" width = "180" align=center />
<img src="ExtraFiles/PhotoFiles/Cfigure3.png" width = "405" align=center />  <br>
<img src="ExtraFiles/PhotoFiles/Cfigure5.png" width = "600" align=center />  <br>

`XXX.c` file  
`gcc factorial.c -o fact`  
`./a.out`  

<img src="ExtraFiles/PhotoFiles/Cfigure6.png" width = "400" align=center />  <br>

➢ Format conversion specifier for a sequence of non-white-space characters is **%s**  
E.g. `scanf(“%s”, a)` will store only “**Comp**” for user input “Comp Sc”  
➢ And **%[^\n]** for strings with _white-space_ included  
E.g. `scanf(“%[^\n]”, a)` will store only “**Comp Sc**” for user input “Comp Sc”  

<img src="ExtraFiles/PhotoFiles/Cfigure7.png" width = "300" align=center />  <br>
strlen() not include the `\0` the indicate termination of a string
