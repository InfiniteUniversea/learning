### Exponent 指数
 （次幂）  
An exponent represents how many times a number is multiplied by itself.

### Fractions 分数  
numerators 分子  
denominators 分母  
square roots 平方根

<img src="ExtraFiles/PhotoFiles/pre-algebra-1.png" width = "500" align=center />  <br>

### Primes 素数 / Prime Numbers 质数
A `product` is the result of multiplying two or more numbers together. For example, 10 is the product of 2 and 5 because 2×5=10.  

### Positive whole numbers 正整数  
factors 因数  

### Factorizations 因式分解

### Factor trees 因式树

### Zero-product property 零积性质

### Order of operations 
is the conventional order in which mathematical operations must be completed: 1) anything grouped together by brackets or parentheses, 2) exponents, 3) multiplication and division (left-to-right), 4) addition and subtraction (left-to-right).
