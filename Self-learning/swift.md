#   Logical Operators

### NOT operator (!)    
inverts a Boolean value, saying, "If NOT this condition, do this."
### AND operator (&&)   
combines two conditions and runs the code only if *both* are true.
### OR operator (||)    
combines two conditions and runs the code if *at least* one is true.

`print("Hello World!")`  
  
`var a = "1"`  variable `var` (changeable assignment)  
`let b = "2"`  constant `let` (unchangeable)   

