> https://www.bilibili.com/video/BV1P7411N7fW/?spm_id_from=333.337.search-card.all.click&vd_source=4e4a722817e8665cdbebf67ab94adac3

# 点集拓扑
### 欧拉定理
> 棱 edge  
> 面 face  
> 顶点 vertex
  
简单版本：  
由多个**n边形**组成的三维多面体中  
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;  `v - e + f = 2` ==> 球  
非**n边形**组成  
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;  `v - e + f = 0` ==> 甜甜圈  
> ##### 甜甜圈的洞 = g  
> g = 0 球  
> g = 1 甜甜圈  
> g = 2 两个洞甜甜圈  
> ... 
 
`v - e + f = 2 - 2g` （g：亏格 Genus）






####


# 代数拓扑
