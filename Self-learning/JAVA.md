

	for(数据类型x  变量y : 遍历对象num)
	{
    	 引用了x的java语句;
	}

---

<img src = "ExtraFiles/PhotoFiles/JAVA1.png" width = "600" align = center /> <br>

**List** -- 元素有序 -- 且元素可重复

## Set集合
**Set** -- 元素无序 -- 且不可重复 -- 没有索引，没有get(),没有set()
  
set接口：也称set集合，但凡是实现了set接口的类都叫做set集合  
特点：元素无素引，元素不可重复(唯一〕  
Hashset集合：实现类--元素存取无序  
		```迭代器遍历 Iterator```  

		Iterator<String> iterator = set1.iterator (); 
		while (iterator.hasNext ()) { 
			System.out.println(iterator.next ());

LinkedHashset集合：实现类--元素存取有序  
Treeset集合：实现类--＞对元素进行排序  
  
注意：  
1.set集合没有特殊的方法，都是使用Co1lection接口的方法  
2.set集合没有素引，所以過历元素的方式就只有：增强for循环，或者选代器  

---

Math.min()比Math.max()大  
  
if(Math.min()<Math.max()){  
	console.log("true");  
}else{  
	console.log("false");  
}  
//输出值：false  
//因为Math.min() 返回 Infinity, 而 Math.max()返回 -Infinity  
————————————————  
这个是python！  
版权声明：本文为CSDN博主「飞起来的大闸蟹」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。  
原文链接：https://blog.csdn.net/baidu_41904590/article/details/120954184  
