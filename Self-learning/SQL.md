### Comparison Operators  

`>` greater than  
`>=` greater than or equal to  
`<` less than  
`<=` less than or equal to  
`=` equality operator  
`!=` `<>` not equal operator  

WHERE : () > and >or

LIKE :  
`%` any number of characters <br>
`_` single character <br>

REGEXP :  
`^` beginning  
`$` end  
`|` logical or  
`[abcd]` `[a-d]`

ORDER BY ... (DESC) : ⬆️ (⬇️)
