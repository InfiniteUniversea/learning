| ✅ | 🟧 |
| ------ | ------ |
| derivative | 导数 |
| heterogeneous | 异质 |
| rare | 稀有 |
| trend | 趋势 |
| well-defined | 明确的 |   
| fundamental | 基本的 |
| cardinality | 基数 |
| partition | 分区 隔离 |
| triage | 分诊	|
| optimal | 最佳 |
| in turn | 反过来 |
| asynchronous | 异步 |
| coalesce | 合并 |
| ------ | ------ |

<img src="ExtraFiles/PhotoFiles/Other-1.png" width = "550" align=center />  <br>
